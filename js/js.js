$(function () {

    let oDatosArticulos = []

    let sku; 
    let articulo;
    let marca;
    let modelo;
    let departamento;
    let clase;
    let familia;
    let stock;
    let cantidad;
    let fechaAlta;
    let fechaBaja;
    let descontinuado;

    var hoy = new Date();
    /****************************************** */

    function fn_infoABC(){
        var webMethod =""
        console.log('Comunicacion...')

        let info = {
            P_sku: sku.option('value'),
            P_descont: descontinuado.option('value'),
            P_marca: marca.option('value'),
            P_modelo: modelo.option('value'),
            P_departamento: departamento.option('value'),
            P_clase: clase.option('value'),
            P_familia: familia.option('value'),
            P_stock: stock.option('value'),
            P_cantidad: cantidad.option('value'),
            P_fechaA: fechaAlta.option('value'),
            P_fechaB: fechaBaja.option('value'),
            P_nombDep: nombreDep.option('value'),
        
        };
        $.ajax({
            type: "POST", 
            url: sUrl, 
            data: JSON.stringify(mis_datos), 
            dataType: "json"
            , contentType: 'application/json; charset=utf-8'
            , success: function (data) {
                const oConfirm = $.parseJSON(data.d);
                $.colorbox.close();
                if (oConfirm.length > 0) {
                    console.log(oConfirm[0]);
                    $(".cont2").fadeOut("slow");
                    $(".cont1").fadeIn("slow");
                    console.log(oConfirm)
                } else {
                    fn_MsjBox("<div id='cbAlerta'><p><b>fn_DetalleABC</b></p><p>" + oConfirm.msj + "</p></div>", true, 400, 300);
                }
            }
            , error: function (xhr, ajaxOptions, thrownError) {
                fn_MsjBox("<div id='cbAlerta'><p><b>fn_Sucursales_Sel</b></p><p>Request failed: " + xhr.responseText + " => " + thrownError + "</p></div>", true, 400, 300);
            }

        });
    }

    function fn_info(P_Sku){
        let info = {
            sku: P_Sku
        };
        $.ajax({
            type: "POST", 
            url: sUrl, 
            data: JSON.stringify(mis_datos),
             dataType: "json"
            , contentType: 'application/json; charset=utf-8'
            , success: function (data) {
                oDatosArticulos = $.parseJSON(data.d);
                
                console.log(oDatosArticulos);

                
            }})


    }

    function fn_DesallesMostrar(){

    }

    function fn_verificarSKU(){

    }
    /****************************************** */
    $("#alta").dxButton({
    type:'sortup',
    icon: 'sortup',
    onClick(){
            $(".cont1").fadeOut("slow");
            $(".cont2").fadeIn("slow");

            sku = $('#text1').dxTextBox({
                value: '',
                readOnly:false
            }).dxTextBox('instance');

            articulo = $('#text2').dxTextBox({
                value: '',
                readOnly:false
            }).dxTextBox('instance');

            marca = $('#text3').dxTextBox({
                value : '',
                readOnly : false
            }).dxTextBox('instance');

            modelo = $('#text4').dxTextBox({
                value : '',
                readOnly : false
            }).dxTextBox('instance');

            departamento = $('#text5').dxSelectBox({
                value : '',
                readOnly : false
            }).dxSelectBox('instance');

            clase = $('#text6').dxSelectBox({
                value : '',
                readOnly : false
            }).dxSelectBox('instance');

            familia = $('#text7').dxSelectBox({
                value : '',
                readOnly : false
            }).dxSelectBox('instance');

            stock = $('#text8').dxTextBox({
                value : '',
                readOnly : false
            }).dxTextBox('instance');

            cantidad = $('#text9').dxTextBox({
                value : '',
                readOnly : false
            }).dxTextBox('instance');

            fechaAlta = $('#text10').dxDateBox({
                displayExpr: 'valor',
                valueExpr: 'valor',
                dataType: 'date',
                onValueChanged: function (e) {
                
                },
            }).dxDateBox('instance');

            fechaBaja = $('#text11').dxDateBox({
                displayExpr: 'valor',
                valueExpr: 'valor',
                onValueChanged: function (e) {
                },
            }).dxDateBox('instance');

            fechaAlta.option('value', hoy);
            fechaBaja.option('value', '1900-01-01')
        },
        }).dxButton('instance');

    $("#baja").dxButton({
        type:'sortdown',
        icon: 'sortdown',
        onClick(){
                $(".cont1").fadeOut("slow");
                $(".cont4").fadeIn("slow");
    
                sku = $('#text1_1').dxTextBox({
                    value: '',
                    readOnly:false
                }).dxTextBox('instance');

                $("#regreso_1").dxButton({
                    type: 'red',
                    icon: 'return',
                    onClick() {
                        $(".cont4").fadeOut("slow");
                        $(".cont1").fadeIn("slow");
                        },
                }).dxButton('instance');

                $("#delete").dxButton({
                    type: 'trash',
                    icon: 'trash',
                    onClick() {
                        $(".cont4").fadeOut("slow");
                        $(".cont1").fadeIn("slow");
            
                        fn_infoABC();
                        },
                }).dxButton('instance');
            
    
              
            },
        }).dxButton('instance');
    
    $("#cambio").dxButton({
        type:'sorted',
        icon: 'sorted',
        onClick(){
                $(".cont1").fadeOut("slow");
                $(".cont5").fadeIn("slow");
    
                sku = $('#text1_3').dxTextBox({
                    value: '',
                    readOnly:false
                }).dxTextBox('instance');
    
                articulo = $('#text2_3').dxTextBox({
                    value: '',
                    readOnly:false
                }).dxTextBox('instance');
    
                marca = $('#text3_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');
    
                modelo = $('#text4_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');
    
                departamento = $('#text5_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');
    
                clase = $('#text6_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');
    
                familia = $('#text7_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');
    
                stock = $('#text8_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');
    
                cantidad = $('#text9_3').dxTextBox({
                    value : '',
                    readOnly : false
                }).dxTextBox('instance');

                fechaAlta = $('#text10_3').dxDateBox({
                    displayExpr: 'valor',
                    valueExpr: 'valor',
                    onValueChanged: function (e) {
                    
                    },
                }).dxDateBox('instance');
    
                fechaBaja = $('#text11_3').dxDateBox({
                    displayExpr: 'valor',
                    valueExpr: 'valor',
                    onValueChanged: function (e) {
                    },
                }).dxDateBox('instance');

                $("#refresh").dxButton({
                    type: 'refresh',
                    icon: 'refresh',
                    onClick() {
            
                        fn_verificarSKU();
                        },
                }).dxButton('instance');

                $("#regresoC").dxButton({
                    type: 'red',
                    icon: 'return',
                    onClick() {
                        $(".cont5").fadeOut("slow");
                        $(".cont1").fadeIn("slow");
                        },
                }).dxButton('instance');
            
                $("#confirmC").dxButton({
                    type: 'check',
                    icon: 'check',
                    onClick() {
                        $(".cont5").fadeOut("slow");
                        $(".cont1").fadeIn("slow");
            
                        fn_infoABC();
                        },
                }).dxButton('instance');

            },
        }).dxButton('instance');

    $("#consult").dxButton({
        type:'search',
        icon: 'search',
        onClick(){
                $(".cont1").fadeOut("slow");
                $(".cont3").fadeIn("slow");
            
                sku = $('#text1_2').dxTextBox({
                    value: '',
                    readOnly:false
                }).dxTextBox('instance');

                $("#grid").dxDataGrid({
                    dataSource: oDatosArticulos,
                    selection: {
                        mode: 'single',
                    },
                    paging: {
                        pageSize: 10
                    },
                    pager: {
                        showPageSizeSelector: true,
                        allowedPageSizes: [10, 25]
                    },
                
                    rowAlternationEnabled: true,
                    showBorders: true,
                    filterRow: { visible: true },

                    columns: [
            
                        {
                            dataField: "P_departamento",
                            dataType: "string",
                            caption: "No.Departamento",
                            allowFiltering: true,
                            alignment: 'center',
                        },
            
                        {
                            dataField: "P_nombDep",
                            dataType: "string",
                            caption: "Nombre departamento",
                            allowFiltering: true,
                            alignment: 'center',
                        },
                        {
                            dataField: "P_clase",
                            caption: "Numero de clase",
                            dataType: "string",
                            alignment: 'center',
                        },
                        {
                            dataField: "P_nombClase",
                            dataType: "string",
                            caption: "Nombre de clase",
                            allowFiltering: true,
                            alignment: 'center',
                        },
                        {
                            dataField: "P_familia",
                            dataType: "string",
                            caption: "Numero Familia",
                            allowFiltering: true,
                            alignment: 'center',
                        },
                        {
                            dataField: "P_nombFamilia",
                            dataType: "string",
                            caption: "Nombre de Familia",
                            allowFiltering: true,
                            alignment: 'center',
                        },
                    ],
                }).dxDataGrid('instance');
               
                $("#regresoG").dxButton({
                    type: 'red',
                    icon: 'return',
                    onClick() {
                        $(".cont3").fadeOut("slow");
                        $(".cont1").fadeIn("slow");
                        },
                }).dxButton('instance');
            },
            
            
        }).dxButton('instance');
    
    descontinuado = $('#descontinuado').dxCheckBox({
        text: 'Descontinuado',
        value: true,
        onValueChanged() {
        },
        });

    $("#regreso").dxButton({
        type: 'red',
        icon: 'return',
        onClick() {
            $(".cont2").fadeOut("slow");
            $(".cont1").fadeIn("slow");
            },
    }).dxButton('instance');

    $("#confirm").dxButton({
        type: 'check',
        icon: 'check',
        onClick() {
            $(".cont2").fadeOut("slow");
            $(".cont1").fadeIn("slow");

            fn_infoABC();
            },
    }).dxButton('instance');

    


})